plugins {
    `java-library`
    idea
    kotlin("jvm")
}

group = "io.highcreeksoftware"
version = "0.1"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("junit", "junit", "4.12")

    val exposedVersion = "0.27.1"
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposedVersion")
    testImplementation("com.h2database:h2:1.4.199")
    testImplementation("org.slf4j:slf4j-simple:1.7.30")
}
