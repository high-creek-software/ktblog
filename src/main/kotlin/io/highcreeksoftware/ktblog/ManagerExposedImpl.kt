package io.highcreeksoftware.ktblog

import io.highcreeksoftware.ktblog.paging.Paginator
import io.highcreeksoftware.ktblog.post.Post
import io.highcreeksoftware.ktblog.post.PostListDto
import io.highcreeksoftware.ktblog.post.PostRepo
import io.highcreeksoftware.ktblog.tag.Tag
import io.highcreeksoftware.ktblog.tag.TagRepo
import io.highcreeksoftware.ktblog.tagpost.TagPostRepo
import java.util.*

class ManagerExposedImpl(val postRepo: PostRepo, val tagRepo: TagRepo, val tagPostRepo: TagPostRepo): Manager {
    override fun storePost(post: Post): Post {
        return postRepo.store(post)
    }

    override fun updatePost(post: Post) {
        postRepo.update(post)
    }

    override fun findPostBySlug(slug: String): Post? = postRepo.fromSlug(slug)

    override fun findPostById(id: UUID): Post? = postRepo.fromId(id)

    override fun countPosts(activeOnly: Boolean): Long = postRepo.count(activeOnly)

    override fun listPosts(activeOnly: Boolean, limit: Int, page: Int): PostListDto {
        var posts = postRepo.listPosts(limit, ((page-1)*limit).toLong(), activeOnly)
        var count = postRepo.count(activeOnly)

        val paginator = getPaginator(count, posts.size, limit, page)

        return PostListDto(posts, count, paginator)
    }

    override fun listFeatured(): List<Post> = postRepo.listFeatured()

    override fun storeTag(tag: Tag): Tag {
        return tagRepo.store(tag)
    }

    override fun updateTag(tag: Tag) {
        tagRepo.update(tag)
    }

    override fun addTagToPost(tagId: UUID, postId: UUID) {
        tagPostRepo.store(tagId, postId)
    }

    override fun removeTagFromPost(postId: UUID, tagId: UUID) {
        tagPostRepo.removeTagFromPost(postId, tagId)
    }

    override fun tagsForPost(postId: UUID): List<Tag> {
        return tagPostRepo.tagsForPost(postId)
    }

    override fun postsForTag(tagId: UUID, limit: Int, page: Int): List<Post> {
        return tagPostRepo.postsForTag(tagId, limit, ((page-1)*limit).toLong())
    }

    override fun postsForTagCount(tagId: UUID): Long = tagPostRepo.postsForTagCount(tagId)

    private fun getPaginator(count: Long, loadedCount: Int, limit: Int, page: Int): Paginator {
        val paginator = Paginator()
        if(loadedCount == limit && (limit * page) < count) {
            paginator.hasMore = true
            paginator.morePage = page + 1
        }
        if(page > 1) {
            paginator.hasLess = true
            paginator.lessPage = page - 1
        }
        return paginator
    }
}