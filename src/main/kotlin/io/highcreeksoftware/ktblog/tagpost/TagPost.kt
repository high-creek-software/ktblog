package io.highcreeksoftware.ktblog.tagpost

import java.time.LocalDateTime
import java.util.*

class TagPost {
    var id: UUID? = null
    var created: LocalDateTime? = null
    var tagId: UUID? = null
    var postId: UUID? = null
}