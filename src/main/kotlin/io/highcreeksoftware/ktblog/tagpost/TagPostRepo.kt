package io.highcreeksoftware.ktblog.tagpost

import io.highcreeksoftware.ktblog.post.Post
import io.highcreeksoftware.ktblog.tag.Tag
import java.util.*

interface TagPostRepo {
    fun store(tagId: UUID, postId: UUID): TagPost
    fun tagsForPost(postId: UUID): List<Tag>
    fun postsForTag(tagId: UUID, limit: Int, offset: Long): List<Post>
    fun postsForTagCount(tagId: UUID): Long
    fun removeTagFromPost(postId: UUID, tagId: UUID)
}