package io.highcreeksoftware.ktblog.tagpost

import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.`java-time`.datetime

object TagPostDsl: UUIDTable("tag_post") {
    var created = datetime("created").nullable()
    val tagId = uuid("tag_id").index()
    val postId = uuid("post_id").index()
}