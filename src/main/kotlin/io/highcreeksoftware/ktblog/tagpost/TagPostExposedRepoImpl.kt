package io.highcreeksoftware.ktblog.tagpost

import io.highcreeksoftware.ktblog.post.Post
import io.highcreeksoftware.ktblog.post.PostDsl
import io.highcreeksoftware.ktblog.tag.Tag
import io.highcreeksoftware.ktblog.tag.TagDsl
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList

class TagPostExposedRepoImpl(val db: Database): TagPostRepo {

    init {
        transaction(db) {
            addLogger(StdOutSqlLogger)
            SchemaUtils.createMissingTablesAndColumns(TagPostDsl)
        }
    }

    override fun store(tagId: UUID, postId: UUID): TagPost {
        val id = transaction(db) {
            addLogger(StdOutSqlLogger)
            TagPostDsl.insertAndGetId {
                it[created] = LocalDateTime.now()
                it[this.tagId] = tagId
                it[this.postId] = postId
            }
        }
        return TagPost().apply {
            this.id = id.value
            this.tagId = tagId
            this.postId = postId
        }
    }

    override fun tagsForPost(postId: UUID): List<Tag> {
        val results: ArrayList<Tag> = ArrayList()
        transaction(db) {
            TagPostDsl.join(TagDsl, joinType = JoinType.INNER, onColumn = TagPostDsl.tagId, otherColumn = TagDsl.id, additionalConstraint = {TagPostDsl.postId eq postId}).slice(TagDsl.id, TagDsl.title, TagDsl.slug, TagDsl.created, TagDsl.updated).selectAll().forEach {
                results.add(Tag().apply {
                    this.id = it[TagDsl.id].value
                    this.title = it[TagDsl.title]
                    this.slug = it[TagDsl.slug]
                    this.created = it[TagDsl.created]
                    this.updated = it[TagDsl.updated]
                })
            }
        }

        return results
    }

    override fun postsForTag(tagId: UUID, limit: Int, offset: Long): List<Post> {
        val posts: ArrayList<Post> = ArrayList()
        transaction(db) {
            TagPostDsl.join(PostDsl, joinType = JoinType.INNER, onColumn = TagPostDsl.postId, otherColumn = PostDsl.id, additionalConstraint = {TagPostDsl.tagId eq tagId}).select(Op.build { PostDsl.active eq true }).limit(limit, offset = offset).forEach {
                posts.add(Post().apply {
                    this.id = it[PostDsl.id].value
                    this.title = it[PostDsl.title]
                    this.slug = it[PostDsl.slug]
                    this.description = it[PostDsl.description]
                })
            }
        }
        return posts
    }

    override fun postsForTagCount(tagId: UUID): Long {
        var postCount = 0L
        transaction(db) {
            postCount = TagPostDsl.select(Op.build { TagPostDsl.tagId eq tagId }).count()
        }
        return postCount
    }

    override fun removeTagFromPost(postId: UUID, tagId: UUID) {
        transaction(db) {
            TagPostDsl.deleteWhere { TagPostDsl.postId eq postId and (TagPostDsl.tagId eq tagId) }
        }
    }
}