package io.highcreeksoftware.ktblog.exts

fun String.slugize(): String {
    return this.replace(" ", "-").toLowerCase()
}