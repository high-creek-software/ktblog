package io.highcreeksoftware.ktblog

import io.highcreeksoftware.ktblog.post.Post
import io.highcreeksoftware.ktblog.post.PostListDto
import io.highcreeksoftware.ktblog.tag.Tag
import java.util.*

interface Manager {
    fun storePost(post: Post): Post
    fun updatePost(post: Post)
    fun findPostBySlug(slug: String): Post?
    fun findPostById(id: UUID): Post?
    fun countPosts(activeOnly: Boolean): Long
    fun listPosts(activeOnly: Boolean, limit: Int = 15, page: Int = 1): PostListDto
    fun listFeatured(): List<Post>

    fun storeTag(tag: Tag): Tag
    fun updateTag(tag: Tag)

    fun addTagToPost(tagId: UUID, postId: UUID)
    fun removeTagFromPost(postId: UUID, tagId: UUID)
    fun tagsForPost(postId: UUID): List<Tag>
    fun postsForTag(tagId: UUID, limit: Int = 15, page: Int = 1): List<Post>
    fun postsForTagCount(tagId: UUID): Long
}