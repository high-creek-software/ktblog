package io.highcreeksoftware.ktblog.post

import java.util.*

interface PostRepo {
    fun store(post: Post): Post
    fun update(post: Post)
    fun fromSlug(slug: String): Post?
    fun fromId(id: UUID): Post?
    fun listPosts(limit: Int, offset: Long, activeOnly: Boolean): List<Post>
    fun listFeatured(): List<Post>
    fun count(activeOnly: Boolean = false): Long
}