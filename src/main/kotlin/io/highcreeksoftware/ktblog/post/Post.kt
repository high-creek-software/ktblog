package io.highcreeksoftware.ktblog.post

import io.highcreeksoftware.ktblog.tag.Tag
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*

class Post: Serializable {
    var id: UUID? = null
    var created: LocalDateTime? = null
    var updated: LocalDateTime? = null

    var title: String = ""
    var slug: String = ""
    var body: String = ""
    var description: String = ""
    var featured: Boolean = false
    var active: Boolean = false
    var authorId: String? = null
    var coverPath: String? = null
    var coverAlt: String? = null

    var ogUrl: String? = null
    var ogTitle: String? = null
    var ogDescription: String? = null
    var ogImage: String? = null
    var ogImageType: String? = null
    var ogImageWidth: String? = null
    var ogImageHeight: String? = null
    var ogType: String? = null
    var ogLocale: String? = null
    var fbAppId: String? = null
    var twitterCard: String? = null
    var twitterSite: String? = null
    var twitterTitle: String? = null
    var twitterDescription: String? = null
    var twitterCreator: String? = null
    var twitterImage: String? = null

    var tags: Array<Tag>? = null
}