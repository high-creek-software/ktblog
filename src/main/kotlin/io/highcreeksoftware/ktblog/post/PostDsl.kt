package io.highcreeksoftware.ktblog.post

import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.`java-time`.datetime

object PostDsl: UUIDTable(name = "posts") {
    val created = datetime("created").nullable()
    val updated = datetime("updated").nullable()
    val title = varchar("title", 512)
    val slug = varchar("slug", 512).index()
    val body = text("body")
    val description = text("description")
    val featured = bool("featured")
    val active = bool("active").index()
    val authorId = varchar("author_id", 48).nullable()
    val coverPath = varchar("cover_path", 2048).nullable()
    val coverAlt = varchar("cover_alt", 2048).nullable()

    val ogUrl = varchar("og_url", 2048).nullable()
    val ogTitle = varchar("og_title", 512).nullable()
    val ogDescription = text("og_description").nullable()
    val ogImage = varchar("og_image", 2048).nullable()
    val ogImageType = varchar("og_image_type", 128).nullable()
    val ogImageWidth = varchar("og_image_width", 64).nullable()
    val ogImageHeight = varchar("og_image_height", 64).nullable()
    val ogType = varchar("og_type", 64).nullable()
    var ogLocale = varchar("og_locale", 64).nullable()
    var fbAppId = varchar("fb_app_id", 256).nullable()
    var twitterCard = varchar("twitter_card", 512).nullable()
    var twitterSite = varchar("twitter_site", 1024).nullable()
    var twitterTitle = varchar("twitter_title", 1024).nullable()
    var twitterDescription = text("twitter_description").nullable()
    var twitterCreator = varchar("twitter_creator", 1024).nullable()
    var twitterImage = varchar("twitter_image", 2048).nullable()
}