package io.highcreeksoftware.ktblog.post

import io.highcreeksoftware.ktblog.paging.Paginator

data class PostListDto(val posts: List<Post>, val totalCount: Long, val paginator: Paginator? = null)