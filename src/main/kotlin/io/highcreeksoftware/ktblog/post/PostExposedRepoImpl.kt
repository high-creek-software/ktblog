package io.highcreeksoftware.ktblog.post

import io.highcreeksoftware.ktblog.exts.slugize
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList

class PostExposedRepoImpl(val db: Database): PostRepo {

    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(PostDsl)
        }
    }

    override fun store(post: Post): Post {
        val id = transaction(db) {
            PostDsl.insertAndGetId {
                it[created] = LocalDateTime.now()
                it[updated] = LocalDateTime.now()
                it[title] = post.title
                it[slug] = post.title.slugize()
                it[body] = post.body
                it[description] = post.description
                it[featured] = post.featured
                it[active] = post.active
                it[authorId] = post.authorId
                it[coverPath] = post.coverPath
                it[coverAlt] = post.coverAlt
            }
        }
        post.id = id.value
        return post
    }

    override fun update(post: Post) {
        transaction(db) {
            PostDsl.update({PostDsl.id eq post.id}) {
                it[updated] = LocalDateTime.now()
                it[title] = post.title
                it[slug] = post.title.slugize()
                it[body] = post.body
                it[description] = post.description
                it[featured] = post.featured
                it[active] = post.active
                it[coverPath] = post.coverPath
                it[coverAlt] = post.coverAlt
                it[ogUrl] = post.ogUrl
                it[ogTitle] = post.ogTitle
                it[ogDescription] = post.ogDescription
                it[ogImage] = post.ogImage
                it[ogImageType] = post.ogImageType
                it[ogImageWidth] = post.ogImageWidth
                it[ogImageHeight] = post.ogImageHeight
                it[ogType] = post.ogType
                it[ogLocale] = post.ogLocale
                it[fbAppId] = post.fbAppId
                it[twitterCard] = post.twitterCard
                it[twitterSite] = post.twitterSite
                it[twitterTitle] = post.twitterTitle
                it[twitterDescription] = post.twitterDescription
                it[twitterCreator] = post.twitterCreator
                it[twitterImage] = post.twitterImage
            }
        }
    }

    override fun fromSlug(slug: String): Post? {
        var post: Post? = null
        transaction(db) {
            val row = PostDsl.select {PostDsl.slug eq slug}.firstOrNull()
            row?.let {
                post = mapPost(it)
            }
        }
        return post
    }

    override fun fromId(id: UUID): Post? {
        var post: Post? = null
        transaction(db) {
            val row = PostDsl.select{PostDsl.id eq id}.firstOrNull()
            row?.let {
                post = mapPost(it)
            }
        }
        return post
    }

    override fun listPosts(limit: Int, offset: Long, activeOnly: Boolean): List<Post> {
        val posts = ArrayList<Post>()
        transaction(db) {
            val query = if(activeOnly) {
                PostDsl.slice(BASIC_COLUMNS.toList()).select{PostDsl.active eq true}
            } else {
                PostDsl.slice(BASIC_COLUMNS.toList()).selectAll()
            }

            query.limit(limit, offset = offset).forEach {
                posts.add(mapBasicPost(it))
            }
        }

        return posts
    }

    override fun listFeatured(): List<Post> {
        val posts = ArrayList<Post>()
        transaction(db) {
            PostDsl.slice(BASIC_COLUMNS.toList()).select(Op.build { PostDsl.active eq true and (PostDsl.featured eq true)}).forEach {
                posts.add(mapBasicPost(it))
            }
        }
        return posts
    }

    override fun count(activeOnly: Boolean): Long {
        var count = 0L
        transaction(db) {
            val query = if(activeOnly) {
                PostDsl.select{PostDsl.active eq true}
            } else {
                PostDsl.selectAll()
            }

            count = query.count()
        }

        return count
    }

    private fun mapPost(it: ResultRow): Post {
        return Post().apply {
            this.id = it[PostDsl.id].value
            this.created = it[PostDsl.created]
            this.updated = it[PostDsl.updated]
            this.title = it[PostDsl.title]
            this.slug = it[PostDsl.slug]
            this.body = it[PostDsl.body]
            this.description = it[PostDsl.description]
            this.featured = it[PostDsl.featured]
            this.active = it[PostDsl.active]
            this.authorId = it[PostDsl.authorId]
            this.coverPath = it[PostDsl.coverPath]
            this.coverAlt = it[PostDsl.coverAlt]

            this.ogUrl = it[PostDsl.ogUrl]
            this.ogTitle = it[PostDsl.ogTitle]
            this.ogDescription = it[PostDsl.ogDescription]
            this.ogImage = it[PostDsl.ogImage]
            this.ogImageType = it[PostDsl.ogImageType]
            this.ogImageWidth = it[PostDsl.ogImageWidth]
            this.ogImageHeight = it[PostDsl.ogImageHeight]
            this.ogType = it[PostDsl.ogType]
            this.ogLocale = it[PostDsl.ogLocale]
            this.fbAppId = it[PostDsl.fbAppId]
            this.twitterCard = it[PostDsl.twitterCard]
            this.twitterSite = it[PostDsl.twitterSite]
            this.twitterTitle = it[PostDsl.twitterTitle]
            this.twitterDescription = it[PostDsl.twitterDescription]
            this.twitterCreator = it[PostDsl.twitterCreator]
            this.twitterImage = it[PostDsl.twitterImage]
        }
    }

    private fun mapBasicPost(it: ResultRow): Post {
        return Post().apply {
            this.id = it[PostDsl.id].value
            this.created = it[PostDsl.created]
            this.updated = it[PostDsl.updated]
            this.title = it[PostDsl.title]
            this.slug = it[PostDsl.slug]
            this.description = it[PostDsl.description]
            this.coverAlt = it[PostDsl.coverAlt]
            this.coverPath = it[PostDsl.coverPath]
            this.featured = it[PostDsl.featured]
            this.active = it[PostDsl.active]
        }
    }

    companion object {
        private val BASIC_COLUMNS = arrayOf(PostDsl.id, PostDsl.created, PostDsl.updated, PostDsl.title, PostDsl.slug, PostDsl.description, PostDsl.coverAlt, PostDsl.coverPath, PostDsl.featured, PostDsl.active)
    }
}