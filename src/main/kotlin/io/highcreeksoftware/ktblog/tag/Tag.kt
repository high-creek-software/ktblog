package io.highcreeksoftware.ktblog.tag

import io.highcreeksoftware.ktblog.post.Post
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*

class Tag: Serializable {
    var id: UUID? = null
    var created: LocalDateTime? = null
    var updated: LocalDateTime? = null

    var title: String = ""
    var slug: String = ""
    var body: String = ""
    var coverPath: String = ""
    var coverAlt: String = ""

    var posts: Array<Post>? = null
}