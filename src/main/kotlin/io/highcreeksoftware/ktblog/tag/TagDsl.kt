package io.highcreeksoftware.ktblog.tag

import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.`java-time`.datetime

object TagDsl: UUIDTable(name = "tags") {
    val created = datetime("created").nullable()
    val updated = datetime("updated").nullable()
    val title = varchar("title", 512)
    val slug = varchar("slug", 512).index()
    val body = text("body")
    val coverPath = varchar("cover_path", 2048)
    val coverAlt = varchar("cover_alt", 2048)
}