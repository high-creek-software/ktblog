package io.highcreeksoftware.ktblog.tag

import io.highcreeksoftware.ktblog.exts.slugize
import io.highcreeksoftware.ktblog.post.PostDsl
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime
import java.util.*

class TagExposedRepoImpl(val db: Database): TagRepo {

    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(TagDsl)
        }
    }

    override fun store(tag: Tag): Tag {
        val id = transaction(db) {
            TagDsl.insertAndGetId {
                it[created] = LocalDateTime.now()
                it[updated] = LocalDateTime.now()
                it[title] = tag.title
                it[slug] = tag.title.slugize()
                it[body] = tag.body
                it[coverPath] = tag.coverPath
                it[coverAlt] = tag.coverAlt
            }
        }
        tag.id = id.value
        return tag
    }

    override fun update(tag: Tag) {
        transaction(db) {
            TagDsl.update({ TagDsl.id eq tag.id }) {
                it[updated] = LocalDateTime.now()
                it[title] = tag.title
                it[slug] = tag.title.slugize()
                it[body] = tag.body
                it[coverPath] = tag.coverPath
                it[coverAlt] = tag.coverAlt
            }
        }
    }

    override fun fromSlug(slug: String): Tag? {
        var tag: Tag? = null
        transaction(db) {
            val row = TagDsl.select{TagDsl.slug eq slug}.firstOrNull()
            row?.let {
                tag = mapTag(it)
            }
        }
        return tag
    }

    override fun fromId(id: UUID): Tag? {
        var tag: Tag? = null
        transaction(db) {
            val row = TagDsl.select{TagDsl.id eq id}.firstOrNull()
            row?.let {
                tag = mapTag(it)
            }
        }
        return tag
    }

    private fun mapTag(it: ResultRow): Tag {
        return Tag().apply {
            this.id = it[TagDsl.id].value
            this.created = it[TagDsl.created]
            this.updated = it[TagDsl.updated]
            this.title = it[TagDsl.title]
            this.slug = it[TagDsl.slug]
            this.body = it[TagDsl.body]
            this.coverPath = it[TagDsl.coverPath]
            this.coverAlt = it[TagDsl.coverAlt]
        }
    }
}