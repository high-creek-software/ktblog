package io.highcreeksoftware.ktblog.tag

import java.util.*

interface TagRepo {
    fun store(tag: Tag): Tag
    fun update(tag: Tag)
    fun fromSlug(slug: String): Tag?
    fun fromId(id: UUID): Tag?
}