package io.highcreeksoftware.ktblog.paging

data class Paginator(var hasMore: Boolean = false, var morePage: Int = 0, var hasLess: Boolean = false, var lessPage: Int = 0)