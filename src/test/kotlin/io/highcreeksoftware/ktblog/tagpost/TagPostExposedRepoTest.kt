package io.highcreeksoftware.ktblog.tagpost

import io.highcreeksoftware.ktblog.post.Post
import io.highcreeksoftware.ktblog.post.PostExposedRepoImpl
import io.highcreeksoftware.ktblog.tag.Tag
import io.highcreeksoftware.ktblog.tag.TagExposedRepoImpl
import org.jetbrains.exposed.sql.Database
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*

class TagPostExposedRepoTest {

    lateinit var db: Database

    @Before
    fun initDb() {
        db = Database.connect("jdbc:h2:mem:regular;DB_CLOSE_DELAY=-1;", driver = "org.h2.Driver")
    }

    @Test
    fun testStoreTagPost() {
        val tagUuid = UUID.randomUUID()
        val postUuid = UUID.randomUUID()
        val tagPostRepo = TagPostExposedRepoImpl(db)

        val tagPost = tagPostRepo.store(tagUuid, postUuid)

        Assert.assertEquals(tagUuid, tagPost.tagId)
        Assert.assertEquals(postUuid, tagPost.postId)
        Assert.assertNotNull(tagPost.id)
    }

    @Test
    fun testTagsForPost() {
        val tagRepo = TagExposedRepoImpl(db)
        val tagPostRepo = TagPostExposedRepoImpl(db)

        val postUUID = UUID.randomUUID()

        var firstTag = Tag().apply {
            title = "First Tag"
        }

        var secondTag = Tag().apply {
            title = "Second Tag"
        }

        var anotherTag = Tag().apply {
            title = "Not for join"
        }

        firstTag = tagRepo.store(firstTag)
        secondTag = tagRepo.store(secondTag)
        anotherTag = tagRepo.store(anotherTag)

        tagPostRepo.store(firstTag.id!!, postUUID)
        tagPostRepo.store(secondTag.id!!, postUUID)
        tagPostRepo.store(anotherTag.id!!, UUID.randomUUID())

        val tags = tagPostRepo.tagsForPost(postUUID)

        Assert.assertEquals(2, tags.size)
    }

    @Test
    fun testPagingPosts() {
        var postRepo = PostExposedRepoImpl(db)
        var tagPostRepo = TagPostExposedRepoImpl(db)

        val tagUUID = UUID.randomUUID()

        var firstPost = Post().apply {
            title = "First Post"
            active = true
        }

        var secondPost = Post().apply {
            title = "Second Post"
            active = true
        }

        firstPost = postRepo.store(firstPost)
        secondPost = postRepo.store(secondPost)

        tagPostRepo.store(tagUUID, firstPost.id!!)
        tagPostRepo.store(tagUUID, secondPost.id!!)

        val posts = tagPostRepo.postsForTag(tagUUID, 10, 0)
        Assert.assertEquals(2, posts.size)
    }
}