package io.highcreeksoftware.ktblog.tagpost

import io.highcreeksoftware.ktblog.post.Post
import io.highcreeksoftware.ktblog.post.PostExposedRepoImpl
import org.jetbrains.exposed.sql.Database
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PostExposedRepoTest {
    lateinit var db: Database

    @Before
    fun initDb() {
        db = Database.connect("jdbc:h2:mem:regular;DB_CLOSE_DELAY=-1;", driver = "org.h2.Driver")
    }

    @Test
    fun testSlugMissing() {
        val postRepo = PostExposedRepoImpl(db)

        val post = postRepo.fromSlug("test-slug")
        Assert.assertNull(post)
    }

    @Test
    fun testSlugExisting() {
        val postRepo = PostExposedRepoImpl(db)

        val insert = Post().apply {
            title = "New Post"
            body = "Dummy post text"
            description = "A dummy post"
            featured = false
            active = true
        }

        postRepo.store(insert)

        val found = postRepo.fromSlug("new-post")
        Assert.assertNotNull(found)
        Assert.assertNotNull(found!!.id)
        Assert.assertEquals(insert.title, found.title)
    }
}